print('############## UNIVERSIDAD NACIONAL DE LOJA ##############')
print('Mantenimiento de Computadoras')
print('Autora: Lilia Susana Tene. Email: lilia.tene@unl.edu.ec')
print('Tema: ASCII')

#Convertir la palabra mantenimiento de mayuculas a minusculas
#Atividad corregida por el docente


upper = "MANTENIMIENTO";
lower = "";
delta = ord('a') - ord('A')
for c in upper:
    lower += chr(ord(c) + delta)

print(lower)